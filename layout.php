<?php
  $bookmarklet = (isset($_GET['source']) && $_GET['source']=='bookmarklet') ? 'class="bookmarklet"' : '';

  $nav = ($bookmarklet == '') ? '<script src="https://framasoft.org/nav/nav.js" type="text/javascript"></script>' : '';

  $meta = '<!DOCTYPE html>
<html '.$bookmarklet.'>
  <head>
    <title>MyFrama</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <script src="shaarli/tpl/frama/inc/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="shaarli/tpl/frama/inc/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="shaarli/tpl/frama/inc/bootstrap/css/bootstrap.min.css" media="all" rel="stylesheet">
    <link href="shaarli/tpl/frama/inc/font-awesome/css/font-awesome.min.css" media="all" rel="stylesheet">
    <link href="shaarli/tpl/frama/inc/frama.css" media="all" rel="stylesheet">
  </head>
  <body>
    '.$nav.'
    <div class="container ombre home">';

  $header = '
      <header>
        <h1 class="big-title"><a href="/"><b class="violet">My</b><b class="vert">Frama</b></a></h1>
        <p class="lead">'.t('Bookmark and share your links').'</p>
        <hr class="trait" role="presentation" />
      </header>';

  $choose1 = '
      <script type="text/javascript">
        jQuery(document).ready(function() {
          jQuery(\'#choose .btn-success\').on(\'click\', function() {
            jQuery(\'#login\').hide();
            jQuery(\'#registration\').slideDown();
          });
          jQuery(\'#choose .btn-primary\').on(\'click\', function() {
            jQuery(\'#registration\').hide();
            jQuery(\'#login\').slideDown();
          });
          jQuery(\'#registration, #login\').hide();
        });
      </script>

      <div class="row" id="choose">
        <div class="col-md-12">
          <p class="text-center"><img src="shaarli/plugins/myframa/screen.jpg" alt="" class="ombre" /></p>
          <br>
          <p class="col-sm-6 text-center">
            <a href="javascript:void(0)" class="btn btn-success btn-lg">
              <i class="fa fa-fw fa-user"></i> '.t('Create an account').'
            </a>
          </p>
          <p class="col-sm-6 text-center">
            <a href="javascript:void(0)" class="btn btn-primary btn-lg">
              <i class="fa fa-fw fa-lock"></i> '.t('Access your account').'
            </a>
          </p>
        </div>
      </div>';

  // Choose {
  $choose2 = '
      <script type="text/javascript">
        jQuery(document).ready(function() {
          jQuery(\'#choose .btn-success\').on(\'click\', function() {
            jQuery(\'#choose\').hide();
            jQuery(\'#registration\').show();
          });
          jQuery(\'#choose .btn-primary\').on(\'click\', function() {
            jQuery(\'#choose\').hide();
            jQuery(\'#login\').show();
          });
          jQuery(\'#registration .btn-default,#login .btn-default\').on(\'click\', function() {
            jQuery(\'#choose\').show();
            jQuery(\'#login, #registration\').hide();
          });
        });
      </script>

      <div class="row" id="choose">
        <div class="col-md-12 text-center">
          <p class="text-center"><a href="javascript:void(0)" class="btn btn-success btn-lg btn-block">
              <i class="fa fa-fw fa-user"></i> '.t('Create an account').'
          </a></p>
          <p>ou</p>
          <p><a href="javascript:void(0)" class="btn btn-primary btn-lg btn-block">
              <i class="fa fa-fw fa-lock"></i> '.t('Access your account').'
          </a></p>
        </div>
      </div>';

  $loggedin = '
      <div class="row">
        <div class="col-md-12">
          <p>'.t('You logged recently with the account').' <b>account</b></p>
          <p><a href="javascript:void(0)" class="btn btn-primary btn-lg">
              <i class="fa fa-fw fa-lock"></i> '.t('Use this account').'
          </a></p>
          <p class="helpblock"><a href="javascript:void(0)" class="btn btn-link">
              '.t('This is not my account').'
          </a></p>
      </div>';

  // Registration
  $active_reg = isset($error) ? 'active' : '';

  $has_error['account'] = $test_values === 'account' ? 'has-error' : '';
  $has_error['email'] = $test_values === 'email' ? 'has-error' : '';
  $has_error['password'] = $test_values === 'password' ? 'has-error' : '';
  $has_error['registration'] = (isset($error))
                             ? '
          <div class="col-md-8 col-md-offset-2">
            <div class="alert alert-danger"><strong>'.t('Heck!').'</strong> '.$error.'</div>
          </div>'
                             : '';

  $value['account'] = isset($_POST['account']) ? $_POST['account'] : '';
  $value['email'] = isset($_POST['email']) ? $_POST['email'] : '';
  $value['password'] = isset($_POST['password']) ? $_POST['password'] : '';

  if(!isset($_GET['source'])) {
    $btn_reg = '
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input class="btn btn-primary" type="submit" value="'.t('Create my account').'" />
                  </div>
                </div>';
  } else {
    $btn_reg = '
                <div class="form-group">
                  <div class="text-right col-xs-12">
                    <a href="javascript:void(0)" class="pull-left btn btn-default">'.t('Cancel').'</a>
                    <input class="btn btn-primary" type="submit" value="'.t('Create my account').'" />
                  </div>
                </div>';
  }

  $registration = '
      <div class="row '.$active_reg.'" id="registration">
        <div class="col-md-12">
          <h2>'.t('Create an account').'</h2>

          '.$has_error['registration'].'

          <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" method="POST" action="#" role="form">
              <div class="alert alert-info">'.t('Please fill out all fields.').'</div>

                <div class="form-group '.$has_error['account'].'">
                  <label class="col-sm-4 control-label" for="account">'.t('Username').'<br /><span class="small">('.t('max. 25 characters').')</span></label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" required="" id="account" name="account" pattern="[a-zA-Z0-9_-]{1,25}" value="'.$value['account'].'" >
                  </div>
                </div>
                <div class="form-group '.$has_error['email'].'">
                  <label class="col-sm-4 control-label" for="email">'.t('Email').'</label>
                  <div class="col-sm-8">
                    <input type="email" class="form-control" required="" id="email" name="email" value="'.$value['email'].'">
                  </div>
                </div>
                <div class="form-group '.$has_error['password'].'">
                  <label class="col-sm-4 control-label" for="password">'.t('Password').'</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" required="" id="password" name="password" value="'.$value['password'].'">
                  </div>
                </div>

                '.$btn_reg.'
            </form>
          </div>
        </div>
      </div>';

  // Login
  $active_log = isset($error_redirection) ? 'active' : '';

  if (isset($error_redirection)) {
    $has_error['login'] = '
      <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-danger"><strong>'.t('Heck!').'</strong> '.$error_redirection.'</div>
      </div>';
  } else {
    $has_error['login'] = '';
  }

  $value['login'] = isset($_POST['subdomain_redirect']) ? $_POST['subdomain_redirect'] : '';

  if(!isset($_GET['source'])) {
    $btn_log = '
              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                  <input class="btn btn-primary" type="submit" value="'.t('Access my account').'" />
                </div>
              </div>';
  } else {
    $btn_log = '
              <div class="form-group">
                <div class="text-right col-xs-12">
                  <a href="javascript:void(0)" class="pull-left btn btn-default">'.t('Cancel').'</a>
                  <input class="btn btn-primary" type="submit" value="'.t('Access my account').'" />
                </div>
              </div>';
  }

  $login = '
      <div class="row '.$active_log.'" id="login">
        <div class="col-md-12">
          <h2>'.t('Do you already have an account?').'</h2>

          '.$has_error['login'].'

          <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" method="POST" action="#" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label" for="subdomain_redirect">'.t('Your account').'</label>
                <div class="col-sm-8">
                  <div class="input-group">
                    <div class="input-group-addon">'.URL_BASE.'/u/</div>
                    <input type="text" class="form-control" required="" id="subdomain_redirect" name="subdomain_redirect" pattern="[a-zA-Z0-9_-]{1,25}" value="'.$value['login'].'">
                  </div>
                </div>
              </div>

              '.$btn_log.'
            </form>
          </div>
        </div>
      </div>';

  $about = '
      <hr role="presentation" />

      <div class="row">
        <div id="tuto-faq" class="col-md-4">
          <h2>'.t('Getting started').'</h2>
          <p class="text-center" role="presentation"><span class="glyphicon glyphicon-question-sign"></span></p>
          <div id="aboutbox">
            <p>'.t('<b class="violet">My</b><b class="vert">Frama</b> is a free online service link sharing.').'</p>
            <ol>
                <li>'.t('Create your account;').'</li>
                <li>'.t('Log into your account;').'</li>
                <li>'.t('Bookmark links and share them;').'</li>
            </ol>
            <p>'.t('Actually, there are ').'<b>'.(iterator_count(new DirectoryIterator(PATH_ACCOUNTS))-2).t(' users registered').'</b>.</p>
          </div>
        </div>

        <div class="col-md-4" id="le-logiciel">
          <h2>'.t('The software').'</h2>
          <p class="text-center" role="presentation"><span class="glyphicon glyphicon-cloud"></span></p>
          <p>'.t('<b class="violet">My</b><b class="vert">Frama</b> is an instance of the software ').'<a href="https://github.com/shaarli/Shaarli/">Shaarli</a> '.t('developed by the Shaarli community from a project initiated by').' <a href="http://sebsauvage.net/">Sébastien Sauvage</a>.</p>
          <p>'.t('Shaarli is under ').'<a href="https://github.com/shaarli/Shaarli/blob/master/COPYING">'.t('ZLIB/LIBPNG license').'</a>.</p>
        </div>

        <div class="col-md-4" id="jardin">
          <h2>'.t('Cultivate your garden').'</h2>
          <p class="text-center" role="presentation"><span class="glyphicon glyphicon-tree-deciduous"></span></p>
          <p>'.t('To participate in the development of the software, suggest improvements or simply download it, visit').'<a href="https://github.com/shaarli/Shaarli/">'.t('the development site').'</a>.</p>
          <p>'.t('If you want to install this software for your own use and thus increase battery life, we help you to:').'</p>
          <p class="text-center"><a href="http://framacloud.org/cultiver-son-jardin/installation-de-shaarli/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> framacloud.org</a></p>
        </div>
      </div>';

  $footer = '
    </div>
  </body>
</html>';

// Accueil full
if($bookmarklet == '') {
  echo $meta.$header.$choose1.$registration.$login.$about.$footer;
// Accueil iframe nav
} else {
  echo $meta.$choose2.$registration.$login.$footer;
}