<?php
// remplacer http par https
define('URL_BASE', (isset($_SERVER['HTTPS']) ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST'].str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname($_SERVER['SCRIPT_FILENAME'])));
define('TITLE', 'MyFrama');

/**
 * Build a directory path by concatenating a list of directory names.
 *
 * You can call this function with as many params as you want.
 * Example: join_path('home', 'test', 'Downloads') returns home/test/Downloads
 *
 * @param string [...] a list of directory names
 * @return a string corresponding to the final pathname
 */
function join_path() {
    $path_parts = func_get_args();
    return join(DIRECTORY_SEPARATOR, $path_parts);
}

define('PATH_ROOT', dirname(__FILE__));
define('PATH_ACCOUNTS', join_path(PATH_ROOT, 'u'));
define('PATH_SHAARLI', join_path(PATH_ROOT, 'shaarli'));

// Init plugins, template and users folders
if ($handle = opendir('plugins')) {
    while (false !== ($file = readdir($handle))) {
        if (!file_exists(join_path(PATH_SHAARLI, 'plugins', $file))) {
            symlink( '../../plugins/' . $file, 'shaarli/plugins/' . $file);
        }
    }
    closedir($handle);
}
if (!file_exists(join_path(PATH_SHAARLI, 'tpl', 'frama'))) {
    symlink( '../../tpl/frama', 'shaarli/tpl/frama');
}
if(!file_exists(PATH_ACCOUNTS)) {
    mkdir('u');
    chmod('u' , 0755);
}
if(file_exists(join_path(PATH_SHAARLI, 'data'))) {
    chmod('shaarli/data' , 0500);
}

require_once( 'setconfig.php');

// Dirty thing to call locale files from 'i18n' and 'myframa' plugins
$GLOBALS['config']['ENABLED_PLUGINS'] = array(
      0 => 'i18n',
      1 => 'myframa'
);
require_once( PATH_SHAARLI.'/plugins/i18n/i18n.php');

/**
 * Test if input values are correct.
 *
 * Tested values are:
 *  - account: max 25 chars from [a-z0-9_-]{1,25}
 *  - password: not empty
 *  - email: valid email address
 * All values are required.
 *
 * @param array $values input values to validate.
 * @return true if $values are correct, false otherwise.
 */
function values_are_valid($values) {
    if ( empty($values['account'])
        || empty($values['password'])
        || empty($values['email'])) {
        return 'empty';
    }

    if (!preg_match('/^[a-z0-9_-]{1,25}?$/', strtolower($values['account']))) {
        return 'account';
    }

    if (strlen($values['password']) <= 0) {
        // Already tested by empty(...) but if one day we want to set
        // a minimal number of chars, we'll just have to change the 0.
        return 'password';
    }

    return true;
}

/**
 * Check if an account is already existing.
 *
 * @param string $account the name of the subdomain to check.
 * @return bool True if the account exists, false otherwise.
 */
function check_account_exists($account) {
    $account_path = join_path(PATH_ACCOUNTS, $account);
    if (file_exists($account_path)) {
        return true;
    } else {
        return false;
    }
}

/**
 * Create a new account.
 *
 * This function creates files under PATH_ACCOUNTS and set the database with
 * correct values.
 * Note values must be verified before calling this function.
 *
 * @param string $account name of the account to create.
 * @param string $password the account password.
 * @param string $email an email address.
 * @throws Exception if any error occured during process.
 */
function create_account($account, $password, $email) {
    $account_path = join_path(PATH_ACCOUNTS, strtolower($account));
    mkdir($account_path);

    $userData    = array('data', 'tmp', 'cache', 'pagecache');
    $blacklist   = array('.', '..', '.git',);

    // Relative symlink to shaarli's content
    if ($handle = opendir('shaarli')) {
        $blacklist = array_merge($blacklist, $userData);

        while (false !== ($file = readdir($handle))) {
            if (!in_array($file, $blacklist)) {
                symlink( '../../shaarli/' . $file, 'u/' . $account . '/' . $file);
            }
        }
        closedir($handle);
    }
    // Create userData folders
    foreach ($userData as $file) {
        mkdir($account_path . '/' . $file);
        chmod('u/' . $account . '/' . $file , 0755);
        copy('shaarli/' . $file . '/.htaccess', 'u/' . $account . '/' . $file . '/.htaccess');
        copy('shaarli/plugins/tags_advanced/example.php', 'u/' . $account . '/data/tags_advanced.php');
        copy('shaarli/plugins/myframa/datastore.php', 'u/' . $account . '/data/datastore.php');
    }
    //--Config writing (from shaarli/index.php install() )--------------
    $GLOBALS['timezone'] = 'Europe/Paris';  // There is no reliable way to guess timezone so we force Paris.;
    // Everything is ok, let's create config file.
    $GLOBALS['login'] = $account;
    $GLOBALS['config']['RECOVERY'] = array (
        'email'   => $email,
        'noreply' => 'no-reply@framasoft.org',
        'attempt' => 0,
        'token'   => ''
    );
    $GLOBALS['salt'] = sha1(uniqid('',true).'_'.mt_rand()); // Salt renders rainbow-tables attacks useless.
    $GLOBALS['hash'] = sha1($password.$GLOBALS['login'].$GLOBALS['salt']);
    $GLOBALS['title'] = 'MyFrama';
    $GLOBALS['config']['ENABLE_UPDATECHECK'] = false;
    $GLOBALS['config']['ENABLED_PLUGINS'] = array(
      0 => 'i18n',
      1 => 'myframa',
      2 => 'recovery',
      3 => 'tags_advanced',
      4 => 'framanav',
      5 => 'addlink_toolbar',
      6 => 'qrcode',
      7 => 'markdown',
    );

    writeConfig($GLOBALS, true, $account_path . '/data/config.php');

}

//------------------------------------------------------------------
// ~~~~~~~~~~~~~~~~~~~~~~ 0. Unset Cookie ~~~~~~~~~~~~~~~~~~~~~~~~~~
if(isset($_GET['cookie']) && $_GET['cookie']=='unset') {
    setcookie('myframa', '', time()+365*24*60*60, dirname($_SERVER["SCRIPT_NAME"]).'/');
    $_COOKIE['myframa'] = '';
}

// ~~~~~~~~~~~~~~~~~~~~~~ 1. Bookmarklet ~~~~~~~~~~~~~~~~~~~~~~~~~~~
function encodeURIComponent($str) {
    $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
    return strtr(rawurlencode($str), $revert);
}

$bm_src= ''; $bm_post = '';
if (isset($_GET['source']) && $_GET['source']=='bookmarklet') {
    $bm_src = '&source=bookmarklet';
    if (isset($_GET['post'])) {
        $bm_url = encodeURIComponent($_GET['post']);
        $bm_title = isset($_GET['title']) ? encodeURIComponent($_GET['title']) : '';
        $bm_description = isset($_GET['description']) ? encodeURIComponent($_GET['description']) : '';
        $bm_post = '&post='.$bm_url.'&title='.$bm_title.'&description='.$bm_description;
    }
    if(isset($_COOKIE['myframa']) && !empty($_COOKIE['myframa'])) {
        $account = $_COOKIE['myframa'];
        header('Location: u/' . $account . '?'.$bm_post.$bm_src);
    }
}

// ~~~~~~~~~~~~~~~~~~~~~~ 2. Account ~~~~~~~~~~~~~~~~~~~~~~~~~~~
$test_values = true;
if (!is_writable(PATH_ACCOUNTS)) {

    $error = t('The directory containing the data must be writable by the web server.');

} elseif (isset($_POST['subdomain_redirect'])) {
    // Handle account redirection.
    $account = strtolower($_POST['subdomain_redirect']);
    if (check_account_exists($account)) {
        // Keep user account in a cookie
        setcookie('myframa', $account, time()+365*24*60*60, dirname($_SERVER["SCRIPT_NAME"]).'/');
        // Redirection
        header('Location: u/' . $account . '?do=login'.$bm_post.$bm_src );
    } else {
        $error_redirection = t('This account does not seem to exist. Are you sure of your account name?');
    }

} elseif (!empty($_POST)) {
    $test_values = values_are_valid($_POST);

    $account_exists = true;
    $error = null;
    $account = strtolower($_POST['account']);
    $password = $_POST['password'];
    $email = $_POST['email'];

    if ($test_values === true) {
        try {
            $account_exists = check_account_exists($account);
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    } else {
        $error = t('Please check the information you entered matches the requested format.');
    }

    if ($account_exists === false) {
        // If $account_exists is false, there was no error catched earlier.
        try {
            create_account($account, $password, $email);
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }

    if (is_null($error) && $account_exists) {
        $error = t('This account appears to already exist, please choose another account name.');
        $test_values = 'account';
    } elseif (is_null($error)) {
        // Keep user account in a cookie
        setcookie('myframa', $account, time()+365*24*60*60, dirname($_SERVER["SCRIPT_NAME"]).'/');
        // Everything is fine, we can redirect user to its new account!
        header('Location: u/' . $account . '?do=login&first'.$bm_post.$bm_src);
    }
}

include 'layout.php';
